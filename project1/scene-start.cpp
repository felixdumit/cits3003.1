#include "Angel.h"
#include <stdlib.h>
#include <dirent.h>
#include <time.h>
#include "bitmap.h"
#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

using namespace std;


/* #STRUCTURES */

typedef struct
{
    int height;
    int width;
    GLubyte *rgbData; // Array of bytes with the colour data for the texture
} Texture;

typedef struct
{
    vec4 loc;
    float scale;
    float angles[3]; // rotations around X, Y and Z axes.
    float diffuse, specular, ambient; // Amount of each light component
    float shine;
    vec3 rgb;
    float brightness; // Multiplies all colours
    int meshId;
    int texId;
    float texScale;
    float dist;
    float speed;
    int direction;
    float movement;
} SceneObject;

typedef Angel::vec4 color4;
typedef Angel::vec4 point4;


/* #GLOBALS */
float totalTime = 0.0;

GLuint program;

// Projection matrix
mat4 projection;

//Rotation matrix
mat4 rotation;

// Distance to move the camera backwards relative to everything else
static float viewDist = 12.0;

// rotation angle - rotates the camera sideways around the centre of the scene.
static float rotSidewaysDeg = 0;

// rotation angle - rotates the camera up and over the centre.
static float rotUpAndOverDeg = 20;

// The actual value is set in the display function.
mat4 view = Translate(0.0, 0.0, 0.0);


GLint windowHeight = 640;

GLint windowWidth = 960;

// For the window title
int numDisplayCalls = 0;

char lab[] = "Project 1";

char *programName = NULL;


// IDs for GLSL variables.
GLuint vPosition, vNormal, vTexCoord, projectionU, modelViewU;


// Stores the path to the models-textures folder.
char dataDir[256];

const int NUM_MESHES = 56;

// A pointer to the mesh to draw, and a VAO for each
aiMesh *meshes[NUM_MESHES];

// Stores IDs from glGenVertexArrays
GLuint vaoIDs[NUM_MESHES];

//*****
GLuint vBoneIDs, vBoneWeights;
GLuint uBoneTransforms;

const aiScene *scenes[NUM_MESHES];
//*****

const int NUM_TEXTURES = 31;

// An array of texture pointers - see getTexture
Texture *textures[NUM_TEXTURES];

// Stores IDs from glGenTextures
GLuint textureIDs[NUM_TEXTURES];

const int MAX_OBJECTS = 2048;

// An array storing the objects currently in the scene.
SceneObject sceneObjs[MAX_OBJECTS];

// How many objects are currenly in the scene.
int objectCount = 0;


static int mouseObject = -1, mouseX = 0, mouseY = 0;

static int moving = 0, currObject = -1;
static int clicked = 0;

static float resizeX, resizeZ;

static int rotObAxis = -1, rotating = 0, currTool = -1;

static float *newVal[4] = {NULL, NULL, NULL, NULL}, old[4], scale[4];


/* #MENUS */

char textureMenuEntries[NUM_TEXTURES][128] =
{
    "1 Plain", "2 Rust", "3 Concrete", "4 Carpet", "5 Beach Sand",
    "6 Rocky", "7 Brick", "8 Water", "9 Paper", "10 Marble",
    "11 Wood", "12 Scales", "13 Fur", "14 Denim", "15 Hessian",
    "16 Orange Peel", "17 Ice Crystals", "18 Grass", "19 Corrugated Iron", "20 Styrofoam",

    "21 Bubble Wrap", "22 Leather", "23 Camouflage", "24 Asphalt", "25 Scratched Ice",
    "26 Rattan", "27 Snow", "28 Dry Mud", "29 Old Concrete", "30 Leopard Skin"
};

char objectMenuEntries[NUM_MESHES][128] =
{
    "1 Walking Gus", "2 Big Dog", "3 Saddle Dinosaur", "4 Dragon", "5 Cleopatra",
    "6 Bone I", "7 Bone II", "8 Rabbit", "9 Long Dragon", "10 Buddha",
    "11 Sitting Rabbit", "12 Frog", "13 Cow", "14 Monster", "15 Sea Horse",
    "16 Head", "17 Pelican", "18 Horse", "19 Kneeling Angel", "20 Porsche I",
    "21 Truck", "22 Statue of Liberty", "23 Sitting Angel", "24 Metal Part", "25 Car",
    "26 Apatosaurus", "27 Airliner", "28 Motorbike", "29 Dolphin", "30 Spaceman",
    "31 Winnie the Pooh", "32 Shark", "33 Crocodile", "34 Toddler", "35 Fat Dinosaur",
    "36 Chihuahua", "37 Sabre-toothed Tiger", "38 Lioness", "39 Fish", "40 Horse (head down)",
    "41 Horse (head up)", "42 Skull", "43 Fighter Jet I", "44 Toad", "45 Convertible",
    "46 Porsche II", "47 Hare", "48 Vintage Car", "49 Fighter Jet II", "50 Gargoyle",
    "51 Chef", "52 Parasaurolophus", "53 Rooster", "54 T-rex", "55 Sphere", "56 Thin Dinosaur"
};


/* #FUNCTIONS */

Texture *loadTexture(char *fileName)
{
    Texture *t = (Texture *) malloc(sizeof (Texture));
    BITMAPINFO *info;

    t->rgbData = LoadDIBitmap(fileName, &info);
    if (t->rgbData == NULL)
    {
        fprintf(stderr, "Error loading image:  %s", fileName);
        exit(1);
    }

    t->height = info->bmiHeader.biHeight;
    t->width = info->bmiHeader.biWidth;

    printf("%d %d\n", t->height, t->width);

    return t;
}

// Loads a texture by number, and binds it for later use.
void loadTextureOnce(int i)
{
    char fileName[220];
    if (i < 0 || i >= NUM_TEXTURES)
    {
        fprintf(stderr, "Error in loading texture - wrong texture number: %d\n", i);
        exit(1);
    }
    if (textures[i] != NULL)
        return;

    CheckError();
    sprintf(fileName, "%s/texture%d.bmp", dataDir, i);

    textures[i] = loadTexture(fileName);

    glActiveTexture(GL_TEXTURE0);
    CheckError();

    // Based on: http://www.opengl.org/wiki/Common_Mistakes
    glBindTexture(GL_TEXTURE_2D, textureIDs[i]);
    CheckError();
    fprintf(stderr, "glTexImage2D(%d, %d, %d, %d, %d, %d, %d, %d, rgbData)\n",
            GL_TEXTURE_2D, 0, GL_RGB, textures[i]->width, textures[i]->height,
            0, GL_RGB, GL_UNSIGNED_BYTE); //, textures[i]->rgbData);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, textures[i]->width, textures[i]->height,
                 0, GL_RGB, GL_UNSIGNED_BYTE, textures[i]->rgbData);

    CheckError();
    //    glEnable(GL_TEXTURE_2D);    // Workaround: http://www.opengl.org/wiki/Hardware_specifics:_ATI
    CheckError();
    glGenerateMipmap(GL_TEXTURE_2D); //Generate num_mipmaps number of mipmaps here.
    CheckError();

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    CheckError();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    CheckError();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    CheckError();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    CheckError();

    glBindTexture(GL_TEXTURE_2D, 0); // Back to default texture
    CheckError();
}

void aiInit()
{
    struct aiLogStream stream;

    // get a handle to the predefined STDOUT log stream and attach
    // it to the logging system. It remains active for all further
    // calls to aiImportFile(Ex) and aiApplyPostProcessing.
    stream = aiGetPredefinedLogStream(aiDefaultLogStream_STDOUT, NULL);
    aiAttachLogStream(&stream);

    // ... same procedure, but this stream now writes the
    // log messages to assimp_log.txt
    stream = aiGetPredefinedLogStream(aiDefaultLogStream_FILE, "assimp_log.txt");
    aiAttachLogStream(&stream);
}

//******************
void initBones(int meshNumber, aiMesh *mesh, const aiScene *scene)
{
    scenes[meshNumber] = scene;

    // Local variables for glBufferData
    GLint boneIDs[mesh->mNumVertices][4];
    GLfloat boneWeights[mesh->mNumVertices][4];

    // Initialize weights to 0.0
    for (unsigned int i = 0; i < mesh->mNumVertices; i++)
        for (int j = 0; j < 4; j++)
            boneWeights[i][j] = 0.0;

    if (mesh->mNumBones == 0)
    {
        for (unsigned int i = 0; i < mesh->mNumVertices; i++)
        {
            boneIDs[i][0] = 0;
            boneWeights[i][0] = 1.0;
        }
    }
    else
    {

        for (unsigned int boneID = 0 ; boneID < mesh->mNumBones ; boneID++)
        {

            for (unsigned int j = 0 ; j < mesh->mBones[boneID]->mNumWeights ; j++)
            {
                int VertexID = mesh->mBones[boneID]->mWeights[j].mVertexId;
                float Weight = mesh->mBones[boneID]->mWeights[j].mWeight;

                // Insertion sort, keeping the 4 largest weights
                for (int slotID = 0; slotID < 4; slotID++)
                {
                    if (boneWeights[VertexID][slotID] < Weight)
                    {
                        for (int shuff = 3; shuff > slotID; shuff--)
                        {
                            boneWeights[VertexID][shuff] = boneWeights[VertexID][shuff - 1];
                            boneIDs[VertexID][shuff] = boneIDs[VertexID][shuff - 1];
                        }
                        boneWeights[VertexID][slotID] = Weight;
                        boneIDs[VertexID][slotID] = boneID;
                        break;
                    }
                }

            }
        }
    }

    GLuint buffers[2];
    glGenBuffers( 2, buffers );

    glBindBuffer( GL_ARRAY_BUFFER, buffers[0] ); CheckError();
    glBufferData( GL_ARRAY_BUFFER, sizeof(int) * 4 * mesh->mNumVertices, boneIDs, GL_STATIC_DRAW ); CheckError();
    glVertexAttribIPointer(vBoneIDs, 4, GL_INT, 0, BUFFER_OFFSET(0)); CheckError();
    glEnableVertexAttribArray(vBoneIDs);     CheckError();

    glBindBuffer( GL_ARRAY_BUFFER, buffers[1] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(float) * 4 * mesh->mNumVertices, boneWeights, GL_STATIC_DRAW );
    glVertexAttribPointer(vBoneWeights, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
    glEnableVertexAttribArray(vBoneWeights);    CheckError();
}
//************


//------Mesh loading ----------------------------------------------------

// The following uses the Open Asset Importer library to load models in .x
// format, including vertex positions, normals, and texture coordinates.
// You shouldn't need to modify this - it's called from drawMesh below.

void loadMeshOnce(int meshNumber)
{

    if (meshNumber >= NUM_MESHES || meshNumber < 0)
    {
        printf("Error - no such  model number");
        exit(1);
    }

    if (meshes[meshNumber] == NULL)
    {
        char filename[256];
        sprintf(filename, "%s/model%d.x", dataDir, meshNumber);
        const aiScene *scene = aiImportFile(filename, aiProcessPreset_TargetRealtime_MaxQuality);


        aiMesh *mesh = scene->mMeshes[0];
        meshes[meshNumber] = mesh;

        glBindVertexArray( vaoIDs[meshNumber] );

        // Create and initialize a buffer object for positions and texture coordinates, initially empty.
        // mesh->mTextureCoords[0] has space for up to 3 dimensions, but we only need 2.
        GLuint buffer[1];
        glGenBuffers( 1, buffer );
        glBindBuffer( GL_ARRAY_BUFFER, buffer[0] );
        glBufferData( GL_ARRAY_BUFFER, sizeof(float) * (3 + 3 + 3)*mesh->mNumVertices,
                      NULL, GL_STATIC_DRAW );

        // Next, we load the position and texCoord data in parts.
        glBufferSubData( GL_ARRAY_BUFFER, 0,
                         sizeof(float) * 3 * mesh->mNumVertices, mesh->mVertices );
        glBufferSubData( GL_ARRAY_BUFFER, sizeof(float) * 3 * mesh->mNumVertices,
                         sizeof(float) * 3 * mesh->mNumVertices, mesh->mTextureCoords[0] );
        glBufferSubData( GL_ARRAY_BUFFER, sizeof(float) * 6 * mesh->mNumVertices,
                         sizeof(float) * 3 * mesh->mNumVertices, mesh->mNormals);

        // Load the element index data
        GLuint elements[mesh->mNumFaces * 3];
        for (GLuint i = 0; i < mesh->mNumFaces; i++)
        {
            elements[i * 3] = mesh->mFaces[i].mIndices[0];
            elements[i * 3 + 1] = mesh->mFaces[i].mIndices[1];
            elements[i * 3 + 2] = mesh->mFaces[i].mIndices[2];
        }

        GLuint elementBufferId[1];
        glGenBuffers(1, elementBufferId);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBufferId[0]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * mesh->mNumFaces * 3, elements, GL_STATIC_DRAW);


        // vPosition it actually 4D - the conversion sets the fourth dimension (i.e. w) to 1.0
        glVertexAttribPointer( vPosition, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
        glEnableVertexAttribArray( vPosition );

        // vTexCoord is actually 2D - the third dimension is ignored (it's always 0.0)
        glVertexAttribPointer( vTexCoord, 3, GL_FLOAT, GL_FALSE, 0,
                               BUFFER_OFFSET(sizeof(float) * 3 * mesh->mNumVertices) );
        glEnableVertexAttribArray( vTexCoord );
        glVertexAttribPointer( vNormal, 3, GL_FLOAT, GL_FALSE, 0,
                               BUFFER_OFFSET(sizeof(float) * 6 * mesh->mNumVertices) );
        glEnableVertexAttribArray( vNormal );

        //**************
        initBones(meshNumber, mesh, scene);
        //**************
        CheckError();
    }

}

static float currRawX()
{
    return ((float)mouseX) / windowWidth;
}

static float currRawZ()
{
    return ((float)mouseY) / windowHeight;
}

static float currX()
{
    if (!moving || currTool == 2) return currRawX();
    return currRawX() * cos(rotSidewaysDeg / 180 * 3.14) - currRawZ() * sin(rotSidewaysDeg / 180 * 3.14);
}

static float currZ()
{
    if (!moving || currTool == 2) return currRawZ();
    return currRawZ() * cos(rotSidewaysDeg / 180 * 3.14) + currRawX() * sin(rotSidewaysDeg / 180 * 3.14);
}

static void clearModes()
{
    mouseObject = -1;
    rotating = 0;
    rotObAxis = -1;
    currTool = -1;
    moving = 0;
    clicked = 0;
}

static void setTool(float *vX, float sX, float *vY, float sY, float *wX, float tX, float *wY, float tY)
{
    if (vX)
    {
        old[0] = *vX;
        newVal[0] = vX;
        scale[0] = sX;
    }
    if (vY)
    {
        old[1] = *vY;
        newVal[1] = vY;
        scale[1] = sY;
    }
    if (wX)
    {
        old[2] = *wX;
        newVal[2] = wX;
        scale[2] = tX;
    }
    if (wY)
    {
        old[3] = *wY;
        newVal[3] = wY;
        scale[3] = tY;
    }
}

static void activateTool(int tool)
{
    currTool = tool;
    resizeX = currX(); resizeZ = currZ();
    if (newVal[tool])
    {
        old[tool] = *newVal[tool];
    }
    if (newVal[tool + 1])
    {
        old[tool + 1] = *newVal[tool + 1];
    }
}

static void clearTool()
{
    currTool = -1;
}

static void mouseClickOrScroll(int button, int state, int x, int y)
{
    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
    {
        clicked = 1;
        if (glutGetModifiers() != GLUT_ACTIVE_SHIFT) activateTool(0);
        else activateTool(2);
    }
    else if (button == GLUT_LEFT_BUTTON && state == GLUT_UP)
    {
        clicked = 0;
        clearTool();
    }
    else if (button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN)
    {
        activateTool(2);
    }
    else if (button == GLUT_MIDDLE_BUTTON && state == GLUT_UP)
    {
        clearTool();
    }
    else if (button == 3)   // scroll up
    {
        viewDist = (viewDist < 0.0 ? viewDist : viewDist * 0.8) - 0.05;
    }
    else if (button == 4)  // scroll down
    {
        viewDist = (viewDist < 0.0 ? viewDist : viewDist * 1.25) + 0.05;
    }

}

static void mousePassiveMotion(int x, int y)
{
    mouseX = x;
    mouseY = y;
}

static void mouseClickMotion(int x, int y)
{
    mouseX = x;
    mouseY = y;

    if (currTool >= 0)
    {
        if (newVal[currTool] != NULL)
        {
            *newVal[currTool] = old[currTool] + (currX() - resizeX) * scale[currTool];
        }
        if (newVal[currTool + 1] != NULL)
        {
            *newVal[currTool + 1] = old[currTool + 1] + (resizeZ - currZ()) * scale[currTool + 1];
        }
        // printf("Mouse movement yields new values: %f %f\n", *newVal[currTool], *newVal[currTool+1]);
        glutPostRedisplay();
    }
}

static void doRotate()
{
    moving = false;
    setTool(&rotSidewaysDeg, 400, &viewDist, -2, &rotSidewaysDeg, 400, &rotUpAndOverDeg, -90);
}


//------Add an object to the scene

static void addObject(int id)
{

    moving = true;
    sceneObjs[objectCount].loc[0] = currX();
    sceneObjs[objectCount].loc[1] = 0.0;
    sceneObjs[objectCount].loc[2] = currZ();
    sceneObjs[objectCount].loc[3] = 1.0;

    if (id != 0 && id != 55)
        sceneObjs[objectCount].scale = 0.005;

    sceneObjs[objectCount].rgb[0] = 0.7;
    sceneObjs[objectCount].rgb[1] = 0.7;
    sceneObjs[objectCount].rgb[2] = 0.7;
    sceneObjs[objectCount].brightness = 1.0;

    sceneObjs[objectCount].diffuse = 1.0;
    sceneObjs[objectCount].specular = 0.5;
    sceneObjs[objectCount].ambient = 0.7;
    sceneObjs[objectCount].shine = 10.0;

    sceneObjs[objectCount].angles[0] = 0.0;
    sceneObjs[objectCount].angles[1] = 180.0;
    sceneObjs[objectCount].angles[2] = 0.0;

    sceneObjs[objectCount].meshId = id;
    sceneObjs[objectCount].texId = rand() % NUM_TEXTURES;
    sceneObjs[objectCount].texScale = 2.0;

    sceneObjs[objectCount].speed = 0.5;
    sceneObjs[objectCount].dist = 1.0;
    sceneObjs[objectCount].movement = 0.0;
    sceneObjs[objectCount].direction = 1;


    currObject = mouseObject = objectCount++;
    setTool(&sceneObjs[currObject].loc[0], 10.0, &sceneObjs[currObject].loc[2], -10.0,
            &sceneObjs[currObject].scale, 0.05, &sceneObjs[currObject].loc[1], 10.0);
    glutPostRedisplay();
}


// ------ The init function

void init( void )
{
    /* initialize random seed: */
    srand ( time(NULL) );

    aiInit();

    for (int i = 0; i < NUM_MESHES; i++)
        meshes[i] = NULL;

    // Allocate a vertex array object for each mesh
    glGenVertexArrays( NUM_MESHES, vaoIDs ); CheckError();

    // Allocate texture objects
    glGenTextures(NUM_TEXTURES, textureIDs); CheckError();

    // Load shaders and use the resulting shader program
    program = InitShader( "vStart.glsl", "fStart.glsl" );

    glUseProgram( program ); CheckError();

    // Initialize the vertex position attribute from the vertex shader
    vPosition = glGetAttribLocation( program, "vPosition" );
    vNormal = glGetAttribLocation( program, "vNormal" ); CheckError();

    // Likewise, initialize the vertex texture coordinates attribute.
    vTexCoord = glGetAttribLocation( program, "vTexCoord" ); CheckError();

    projectionU = glGetUniformLocation(program, "Projection");
    modelViewU = glGetUniformLocation(program, "ModelView");


    //***************
    vBoneIDs = glGetAttribLocation( program, "BoneIDs" );    CheckError();
    vBoneWeights = glGetAttribLocation( program, "BoneWeights" );    CheckError();
    uBoneTransforms = glGetUniformLocation( program, "BoneTransforms" );    CheckError();
    //***************


    // Objects 0, and 1 are the ground and the first light.
    addObject(0); // Square for the ground
    sceneObjs[0].loc = vec4(0.0, 0.0, 0.0, 1.0);
    sceneObjs[0].scale = 10.0;
    sceneObjs[0].angles[0] = 90.0; // Rotate it.
    sceneObjs[0].texScale = 5.0; // Repeat the texture.

    addObject(55); // Sphere for the first light
    sceneObjs[1].loc = vec4(2.0, 1.0, 1.0, 1.0);
    sceneObjs[1].scale = 0.1;
    sceneObjs[1].texId = 0; // Plain texture
    sceneObjs[1].brightness = 0.2; // The light's brightness is 5 times this (below).


    // adding second light
    addObject(55);
    sceneObjs[2].loc = vec4(-2.0, 1.0, 1.0, 1.0);
    sceneObjs[2].scale = 0.2;
    sceneObjs[2].texId = 0;
    sceneObjs[2].rgb[0] = 1.0;
    sceneObjs[2].rgb[1] = 1.0;
    sceneObjs[2].rgb[2] = 0.9;
    sceneObjs[2].brightness = 0.5;

    //add Gus
    addObject(1);//rand() % NUM_MESHES); // Add one scene object
    sceneObjs[3].loc += vec4(0, 0, -0.5, 0);

    // We need to enable the depth test to discard fragments that
    // are behind previously drawn fragments for the same pixel.
    glEnable( GL_DEPTH_TEST );
    doRotate(); // Start in camera rotate mode.
    glClearColor( 0.0, 0.0, 0.0, 1.0 ); /* black background */
}

//***********************
void setPose(int meshID, float currentTime)
{
    aiMesh *mesh = meshes[meshID];
    const aiScene *scene = scenes[meshID];

    if (mesh->mNumBones == 0)
    {
        mat4 idTrans[1] = { mat4(1.0) };
        glUniformMatrix4fv(uBoneTransforms, 1, GL_TRUE, (const GLfloat *)idTrans);
        return;

    }

    if (scene->mNumAnimations > 0)
    {
        aiAnimation *anim = scene->mAnimations[0];  // Only show the first animation

        // Set transforms from bone channels

        for (unsigned int chanID = 0; chanID < anim->mNumChannels; chanID++)
        {
            aiNodeAnim *channel = anim->mChannels[chanID];

            // Based on http://sourceforge.net/projects/assimp/forums/forum/817654/topic/3880745
            // and http://ogldev.atspace.co.uk/www/tutorial38/tutorial38.html

            aiVector3D curPosition;
            aiQuaternion curRotation;
            // scaling purposefully left out

            // find the node which the channel affects
            aiNode *targetNode = scene->mRootNode->FindNode( channel->mNodeName);

            // find current position
            size_t posIndex = 0;
            while ( 1 )
            {
                // break if this is the last key - there are no more keys after this one, we need to use it
                if ( posIndex + 1 >= channel->mNumPositionKeys )
                    break;

                // break if the next key lies in the future - the current one is the correct one then
                if ( channel->mPositionKeys[posIndex + 1].mTime > currentTime )
                    break;

                posIndex++;
            }

            // This assumes that there is at least one key
            if (posIndex + 1 == channel-> mNumPositionKeys)
                curPosition = channel->mPositionKeys[posIndex].mValue;
            else
            {
                float t0 = channel->mPositionKeys[posIndex].mTime;
                float t1 = channel->mPositionKeys[posIndex + 1].mTime;
                float weight1 = (currentTime - t0) / (t1 - t0);
                float weight0 = 1.0 - weight1;

                curPosition = channel->mPositionKeys[posIndex].mValue * weight0 +
                              channel->mPositionKeys[posIndex + 1].mValue * weight1;
            }

            // same goes for rotation
            size_t rotIndex = 0;
            while ( 1 )
            {
                if ( rotIndex + 1 >= channel->mNumRotationKeys )
                    break;

                if ( channel->mRotationKeys[rotIndex + 1].mTime > currentTime )
                    break;

                rotIndex++;
            }
            if (rotIndex + 1 == channel-> mNumRotationKeys)
                curRotation = channel->mRotationKeys[rotIndex].mValue;
            else
            {
                float t0 = channel->mRotationKeys[rotIndex].mTime;
                float t1 = channel->mRotationKeys[rotIndex + 1].mTime;
                float weight1 = (currentTime - t0) / (t1 - t0);
                //float weight0 = 1.0 - weight1;

                // Interpolate using quaternions
                aiQuaternion::Interpolate(curRotation, channel->mRotationKeys[rotIndex].mValue,
                                          channel->mRotationKeys[rotIndex + 1].mValue, weight1);
                curRotation = curRotation.Normalize();
            }

            // now build a transformation matrix from it. First rotation, then push position into it.
            aiMatrix4x4 trafo = aiMatrix4x4(curRotation.GetMatrix());
            trafo.a4 = curPosition.x; trafo.b4 = curPosition.y; trafo.c4 = curPosition.z;

            // assign this transformation to the node
            targetNode->mTransformation = trafo;
        }

    }

    mat4 boneTransforms[mesh->mNumBones];

    for (unsigned int a = 0; a < mesh->mNumBones; a++)
    {

        const aiBone *bone = mesh->mBones[a];

        // find the corresponding node by again looking recursively through the node hierarchy for the same name
        aiNode *node = scene->mRootNode->FindNode( bone->mName);

        // start with the mesh-to-bone matrix
        aiMatrix4x4 b = bone->mOffsetMatrix;

        // and now append all node transformations down the parent chain until we're back at object coordinates again
        const aiNode *tempNode = node;
        while ( tempNode)
        {
            b = tempNode->mTransformation * b;
            tempNode = tempNode->mParent;
        }
        boneTransforms[a] =  mat4(vec4(b.a1, b.a2, b.a3, b.a4),
                                  vec4(b.b1, b.b2, b.b3, b.b4),
                                  vec4(b.c1, b.c2, b.c3, b.c4),
                                  vec4(b.d1, b.d2, b.d3, b.d4));
    }

    glUniformMatrix4fv(uBoneTransforms, mesh->mNumBones, GL_TRUE, (const GLfloat *)boneTransforms);
}
//returns (-1 ~ 1) * distance
//t = time, v = velocity, d = distance, w= angular vel., k= num. fourier iterations
float triangular_wave(float t, float v, float d)
{
    float pi = 3.14159265;
    float w  = 2 * pi * v / d;
    int i = 0;
    int k = 20;
    float x = 0;
    //fourier series first k terms
    for (i = 0; i < k; i++)
        x += (pow(-1, i)) * ((sin(t * w * (2 * i + 1))) / (pow(2 * i + 1, 2)));

    return (8 / pow(pi, 2)) * x * d;

}
//----------------------------------------------------------------------------
void drawMesh(SceneObject sceneObj)
{

    // Activate a texture, loading if needed.
    loadTextureOnce(sceneObj.texId);
    glActiveTexture(GL_TEXTURE0 );
    glBindTexture(GL_TEXTURE_2D, textureIDs[sceneObj.texId]);

    // Texture 0 is the only texture type in this program, and is for the rgb colour of the
    // surface but there could be separate types for, e.g., specularity and normals.
    glUniform1i( glGetUniformLocation(program, "texture"), 0 );

    // Set the texture scale for the shaders
    glUniform1f( glGetUniformLocation( program, "texScale"), sceneObj.texScale );


    // Set the projection matrix for the shaders
    glUniformMatrix4fv( projectionU, 1, GL_TRUE, projection );


    //get the global time and set animation time acording to distance
    float ttime =  glutGet(GLUT_ELAPSED_TIME) / 1000.0;
    //anim time is a value from 0 to 1
    float animTime = triangular_wave(ttime, sceneObj.speed, 0.5) + 0.5;

    vec4 walkVec = vec4(0, 0, 0, 0);

    //if its Gus
    if (sceneObj. meshId == 1  )
    {
        //TODO add walking code here (depending on speed & distance)
        //get angles of rotatin along the ground and up or down
        float angleGround = 3.1415 * (sceneObj.angles[1] - 180) / 180;
        float angleUp = 3.1415 * (sceneObj.angles[0]) / 180;

        // get current position relative to time, speed and distance
        float movedist = triangular_wave(ttime, sceneObj.speed, sceneObj.dist);

        //vector that represent the amount gus needs to walk
        walkVec = (movedist) * vec4(sin(angleGround), -sin(angleUp), cos(angleGround), 0);
    }

    // Set the model matrix - this should combine translation, rotation and scaling based on what's
    // in the sceneObj.structure (see near the top of the program).
    //DONE: rotate the objects aswell as the scene
    //DONE: added walkVec
    mat4 model = Translate(sceneObj.loc + walkVec) * Scale(sceneObj.scale)
                 * RotateX(sceneObj.angles[0]) * RotateY(sceneObj.angles[1]) * RotateZ(sceneObj.angles[2]);
    // Set the model-view matrix for the shaders
    glUniformMatrix4fv( modelViewU, 1, GL_TRUE, view * model );

    // Activate the VAO for a mesh, loading if needed.
    loadMeshOnce(sceneObj.meshId); CheckError();

    // adjust to number of frames in the animation
    int frames = 20;
    setPose(sceneObj.meshId,  animTime * frames );
    //**************
    glBindVertexArray( vaoIDs[sceneObj.meshId] ); CheckError();

    glDrawElements(GL_TRIANGLES, meshes[sceneObj.meshId]->mNumFaces * 3, GL_UNSIGNED_INT, NULL); CheckError();
}


void
display( void )
{
    numDisplayCalls++;

    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    CheckError(); // May report a harmless GL_INVALID_OPERATION with GLEW on the first frame


    // Set the view matrix.  To start with this just moves the camera backwards.  You'll need to
    // add appropriate rotations.

    //DONE: added rotations
    rotation = RotateX(rotUpAndOverDeg) * RotateY(rotSidewaysDeg);

    view = Translate(0.0, 0.0, -viewDist) * rotation;


    SceneObject lightObj1 = sceneObjs[1]; // The actual light is just in front of the sphere.
    point4 lightPosition = view * ( lightObj1.loc - normalize(lightObj1.loc) * lightObj1.scale * 1.05 );
    glUniform4fv( glGetUniformLocation(program, "LightPosition"), 1, lightPosition); CheckError();

    SceneObject lightObj2 = sceneObjs[2]; // 2nd light
    point4 GlobalLightPosition = view * ( lightObj2.loc - normalize(lightObj2.loc) * lightObj2.scale * 1.05 );
    glUniform4fv( glGetUniformLocation(program, "GlobalLightPosition"), 1, GlobalLightPosition); CheckError();

    for (int i = 0; i < objectCount; i++)
    {
        SceneObject so = sceneObjs[i];

        vec3 rgb = so.rgb * lightObj1.rgb * so.brightness * lightObj1.brightness * 2.0;
        glUniform3fv( glGetUniformLocation(program, "AmbientProduct"), 1, so.ambient * rgb ); CheckError();
        glUniform3fv( glGetUniformLocation(program, "DiffuseProduct"), 1, so.diffuse * rgb );
        glUniform3fv( glGetUniformLocation(program, "SpecularProduct"), 1, so.specular * rgb);
        vec3 rgb2 = so.rgb * lightObj2.rgb * so.brightness * lightObj2.brightness * 2.0;
        glUniform3fv( glGetUniformLocation(program, "GlobalAmbientProduct"), 1, so.ambient * rgb2 ); CheckError();
        glUniform3fv( glGetUniformLocation(program, "GlobalDiffuseProduct"), 1, so.diffuse * rgb2 );
        glUniform3fv( glGetUniformLocation(program, "GlobalSpecularProduct"), 1, so.specular * rgb2 );


        glUniform1f( glGetUniformLocation(program, "Shininess"), so.shine ); CheckError();

        drawMesh(sceneObjs[i]);

    }

    glutSwapBuffers();

}

//--------------Menus

static void objectMenu(int id)
{
    clearModes();
    addObject(id);
}

static void texMenu(int id)
{
    clearModes();
    if (currObject >= 0)
    {
        sceneObjs[currObject].texId = id;
        glutPostRedisplay();
    }
}

static void groundMenu(int id)
{
    clearModes();
    sceneObjs[0].texId = id;
    glutPostRedisplay();
}

static void lightMenu(int id)
{
    clearModes();
    if (id == 70)
    {
        moving = 1;
        setTool(&sceneObjs[1].loc[0], 10.0, &sceneObjs[1].loc[2], -10.0,
                &sceneObjs[1].brightness, 1.0, &sceneObjs[1].loc[1], 10.0);

    }
    else if (id == 71)
    {
        setTool(&sceneObjs[1].rgb[0], 1.0, &sceneObjs[1].rgb[1], 1.0,
                &sceneObjs[1].rgb[2], 1.0, &sceneObjs[1].brightness, 1.0);
    }
    else if (id == 80)
    {
        moving = 1;
        setTool(&sceneObjs[2].loc[0], 10.0, &sceneObjs[2].loc[2], -10.0,
                &sceneObjs[2].brightness, 1.0, &sceneObjs[2].loc[1], 10.0);
    }
    else if (id == 81)
    {
        setTool(&sceneObjs[2].rgb[0], 1.0, &sceneObjs[2].rgb[1], 1.0,
                &sceneObjs[2].rgb[2], 1.0, &sceneObjs[2].brightness, 1.0);
    }

    else
    {
        printf("Error in lightMenu\n");
        exit(1);
    }
}

static int createArrayMenu(int size, const char menuEntries[][128], void(*menuFn)(int))
{
    int nSubMenus = (size - 1) / 10 + 1;
    int subMenus[nSubMenus];
    for (int i = 0; i < nSubMenus; i++)
    {
        subMenus[i] = glutCreateMenu(menuFn);
        for (int j = i * 10 + 1; j <= min(i * 10 + 10, size); j++)
            glutAddMenuEntry( menuEntries[j - 1] , j); CheckError();
    }
    int menuId = glutCreateMenu(menuFn);
    for (int i = 0; i < nSubMenus; i++)
    {
        char num[6];
        sprintf(num, "%d-%d", i * 10 + 1, min(i * 10 + 10, size));
        glutAddSubMenu(num, subMenus[i]); CheckError();
    }
    return menuId;
}

static void materialMenu(int id)
{
    clearModes();
    if (currObject < 0) return;
    if (id == 10) setTool(&sceneObjs[currObject].rgb[0], 1.0, &sceneObjs[currObject].rgb[1], 1.0,
                              &sceneObjs[currObject].rgb[2], 1.0, &sceneObjs[currObject].brightness, 1.0);


    //DONE: added ambient, diffuse, specular, shine. Check the float values maybe
    if (id == 20) setTool(&sceneObjs[currObject].ambient, 1.0, &sceneObjs[currObject].diffuse, 1.0,
                              &sceneObjs[currObject].specular, 1.0, &sceneObjs[currObject].shine, 100);




    else
    {
        printf("Error in materialMenu\n");
    }
}

static void moveMenu(int id)
{
    clearModes();
    if (currObject < 0) return;

    //Change speed and distance of walk
    if (id == 99)
    {
        moving = 1;
        setTool(&sceneObjs[currObject].speed, 2, &sceneObjs[currObject].dist, 10, 0, 0, 0, 0);
    }



    else
    {
        printf("Error in moveMenu\n");
    }
}

static void mainmenu(int id)
{
    clearModes();
    if (id == 41 && currObject >= 0)
    {
        moving = 1;
        setTool(&sceneObjs[currObject].loc[0], 10, &sceneObjs[currObject].loc[2], -10,
                &sceneObjs[currObject].scale, 0.05, &sceneObjs[currObject].loc[1], 10);
    }
    if (id == 50)
        doRotate();
    if (id == 55 && currObject >= 0)
    {
        setTool(&sceneObjs[currObject].angles[1], 400, &sceneObjs[currObject].angles[0], -400,
                &sceneObjs[currObject].angles[2], 400, &sceneObjs[currObject].texScale, 15);
    }
    if (id == 99) exit(0);
}

static void makeMenu()
{
    int objectId = createArrayMenu(NUM_MESHES, objectMenuEntries, objectMenu);

    int materialMenuId = glutCreateMenu(materialMenu);
    glutAddMenuEntry("R/G/B/All", 10);
    glutAddMenuEntry("Ambient/Diffuse/Specular/Shine", 20);

    int texMenuId = createArrayMenu(NUM_TEXTURES, textureMenuEntries, texMenu);
    int groundMenuId = createArrayMenu(NUM_TEXTURES, textureMenuEntries, groundMenu);

    int lightMenuId = glutCreateMenu(lightMenu);
    glutAddMenuEntry("Move Light 1", 70);
    glutAddMenuEntry("R/G/B/All Light 1", 71);
    glutAddMenuEntry("Move Light 2", 80);
    glutAddMenuEntry("R/G/B/All Light 2", 81);

    int moveMenuId = glutCreateMenu(moveMenu);
    glutAddMenuEntry("Set walking distance/speed", 99);

    glutCreateMenu(mainmenu);
    glutAddMenuEntry("Rotate/Move Camera", 50);
    glutAddSubMenu("Add object", objectId);
    glutAddMenuEntry("Position/Scale", 41);
    glutAddMenuEntry("Rotation/Texture Scale", 55);
    glutAddSubMenu("Material", materialMenuId);
    glutAddSubMenu("Walking", moveMenuId);
    glutAddSubMenu("Texture", texMenuId);
    glutAddSubMenu("Ground Texture", groundMenuId);
    glutAddSubMenu("Lights", lightMenuId);
    glutAddMenuEntry("EXIT", 99);
    glutAttachMenu(GLUT_RIGHT_BUTTON);
}


//----------------------------------------------------------------------------

void
keyboard( unsigned char key, int x, int y )
{
    switch ( key )
    {
    case 033:
        exit( EXIT_SUCCESS );
        break;
    }
}

//----------------------------------------------------------------------------


void idle( void )
{
    glutPostRedisplay();
}



void reshape( int width, int height )
{

    windowWidth = width;
    windowHeight = height;

    glViewport(0, 0, width, height);

    // You'll need to modify this so that the view is similar to that in the sample solution.
    // In particular:
    //   - the view should be able to get "closer" to objects (not as easy as it might look) --
    //   - when the width is less than the height, the view should adjust so that the same part
    //     of the scene is visible across the width of the window. -- DONE

    fprintf(stderr, "W:%d H:%d\n", width, height);
    fflush(stderr);

    //bring near plane as close as possible
    GLfloat nearDist = -0.3f;
    float hratio = ((float) width) / ((float) height);
    projection = Perspective(11.25, hratio, nearDist, 100);


}


void timer(int unused)
{
    char title[256];
    sprintf(title, "%s %s: %d Frames Per Second @ %d x %d",
            lab, programName, numDisplayCalls, windowWidth, windowHeight );

    glutSetWindowTitle(title);

    fprintf(stderr, "Speed:%f  Dist:%f\n", sceneObjs[currObject].speed, sceneObjs[currObject].dist);
    fflush(stderr);

    numDisplayCalls = 0;
    glutTimerFunc(1000, timer, 1);
}

char dirDefault1[] = "models-textures";
char dirDefault2[] = "/cslinux/examples/CITS3003/project-files/models-textures";

void fileErr(char *fileName)
{
    printf("Error reading file: %s\n", fileName);
    printf("When not in the CSSE labs, you will need to include the directory containing\n");
    printf("the models on the command line, or put it in the same folder as the exectutable.");
    exit(1);
}

int main( int argc, char *argv[] )
{

    // Get the program name, excluding the directory, for the window title
    programName = argv[0];
    for (char *cpointer = argv[0]; *cpointer != 0; cpointer++)
        if (*cpointer == '/' || *cpointer == '\\') programName = cpointer + 1;

    // Set the models-textures directory, via the first argument or two defaults.
    if (argc > 1)
        strcpy(dataDir, argv[1]);
    else if (opendir(dirDefault1))
        strcpy(dataDir, dirDefault1);
    else if (opendir(dirDefault2))
        strcpy(dataDir, dirDefault2);
    else fileErr(dirDefault1);


    glutInit( &argc, argv );
    glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH );
    glutInitWindowSize( windowWidth, windowHeight );
    // glutInitWindowPosition( 512, 0);

    glutInitContextVersion( 3, 2);
    //glutInitContextProfile( GLUT_CORE_PROFILE );
    glutInitContextProfile( GLUT_COMPATIBILITY_PROFILE );

    glutCreateWindow( "Wait a sec.." );

    glewInit(); // May cause GL_INVALID_ENUM due to a bug, but harmless with glewExperimental
    CheckError(); // See: http://www.opengl.org/wiki/OpenGL_Loading_Library

    //glutFullScreen();
    init(); CheckError();

    glutDisplayFunc( display );
    glutKeyboardFunc( keyboard );
    glutIdleFunc( idle );

    glutMouseFunc( mouseClickOrScroll );
    glutMotionFunc(mouseClickMotion);
    glutPassiveMotionFunc(mousePassiveMotion);

    glutReshapeFunc( reshape );
    glutTimerFunc(1000, timer, 1); CheckError();

    makeMenu(); CheckError();

    glutMainLoop();
    return 0;
}