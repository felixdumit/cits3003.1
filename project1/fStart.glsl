#version 150

in vec3 pos;
in vec3 normal;
in  vec2 texCoord;  // The third coordinate is always 0.0 and is discarded

out vec4 color;

uniform sampler2D texture;

uniform vec3 AmbientProduct, DiffuseProduct, SpecularProduct;
uniform vec3 GlobalAmbientProduct, GlobalDiffuseProduct, GlobalSpecularProduct;
uniform mat4 ModelView;
uniform mat4 Projection;
uniform vec4 LightPosition;
uniform vec4 GlobalLightPosition;
uniform float Shininess;


void
main()
{
    // The vector to the light from the vertex
    vec3 Lvec = LightPosition.xyz - pos;

    // Unit direction vectors for Blinn-Phong shading calculation
    vec3 L = normalize( Lvec );   // Direction to the light source
    vec3 E = normalize( -pos );   // Direction to the eye/camera
    vec3 H = normalize( L + E );  // Halfway vector

    // Transform vertex normal into eye coordinates (assumes scaling is uniform across dimensions)
    vec3 N = normalize( (ModelView*vec4(normal, 0.0)).xyz );

    // Compute terms in the illumination equation
    vec3 ambient = AmbientProduct;

    float Kd = max( dot(L, N), 0.0 );
    vec3  diffuse = Kd*DiffuseProduct;

    float Ks = pow( max(dot(N, H), 0.0), Shininess );
    vec3  specular = Ks * SpecularProduct;

    if( dot(L, N) < 0.0 ) {
        specular = vec3(0.0, 0.0, 0.0);
    }
    //shine towards white by making the color a 'multiple' of white
    else
        specular = vec3(1,1,1)* max( max(specular.r,specular.g), max(specular.g, specular.b) );


    //calculates distance and sets parameters for the light reduction
    float dist = dot(Lvec,Lvec);
    float a=0.15;
    float b=0.025;
    color.rgb = (ambient + diffuse + specular)/(a*sqrt(dist) + b*dist);
    color.a = 1.0;


    // The vector to the second light from the origin
    vec3 gLvec = GlobalLightPosition.xyz - (ModelView * vec4(0.0, 0.0, 0.0, 1.0)).xyz;

    // Unit direction vectors for Blinn-Phong shading calculation
    vec3 gL = normalize( gLvec );   // Direction to the light source
    vec3 gH = normalize( gL + E );  // Halfway vector

    // Compute terms in the illumination equation
    ambient = GlobalAmbientProduct;

    float gKd = max( dot(gL, N), 0.0 );
    diffuse = gKd*GlobalDiffuseProduct;

    float gKs = pow( max(dot(N, gH), 0.0), Shininess );
    specular = gKs * GlobalSpecularProduct;

    if( dot(gL, N) < 0.0 ) {
        specular = vec3(0.0, 0.0, 0.0);
    }
    //shine towards white by making the color a 'multiple of' white
    else
        specular = vec3(1,1,1)* max( max(specular.r,specular.g), max(specular.g, specular.b) );


    color.rgb += ambient + diffuse + specular;

    color *= texture2D( texture, texCoord * 2.0 );
}