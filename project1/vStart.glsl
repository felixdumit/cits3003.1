#version 150

in  vec4 vPosition;
in  vec3 vNormal;
in  vec2 vTexCoord;

out vec3 pos;
out vec3 normal;
out vec2 texCoord;

uniform mat4 Projection;
uniform mat4 ModelView;

//*******
in  ivec4 BoneIDs;
in   vec4 BoneWeights;

const int MAX_BONES = 128;
uniform mat4 BoneTransforms[MAX_BONES];
//*******

void main()
{

    //***************
    mat4 BoneTransform =  BoneTransforms[BoneIDs[0]] * BoneWeights[0];
                        + BoneTransforms[BoneIDs[1]] * BoneWeights[1];
                        + BoneTransforms[BoneIDs[2]] * BoneWeights[2];
                        + BoneTransforms[BoneIDs[3]] * BoneWeights[3];

    //BoneTransform = mat4(1.0);  // For debugging
    //BoneTransform = BoneTransforms[BoneIDs[0]] * BoneWeights[0];  // For debugging

    vec4 btPosition = BoneTransform * vPosition;
    vec3 btNormal = mat3(BoneTransform) * vNormal;
    //*************


    // Transform vertex position into eye coordinates
    pos = (ModelView * btPosition).xyz;
    normal = btNormal;

    gl_Position = Projection * ModelView * btPosition;
    texCoord = vTexCoord;
}