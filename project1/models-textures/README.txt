Images of the models are given in models.jpg

Every model*.txt file contains a header specifying the number of 
vertices and the number of polygons (which are all triangles).

The header is followed by list of vertices and then polygon indices.

Each line in the vertex list contains the xyz vertex coordinates and the xyz components of its unit normal.

The polygon list simply contains which three vertices make a polygon.



**************************** COPYRIGHT NOTICE *****************************************

These models have been downloaded from the Internet or reconstructed from data downloaded 
from the Internet. Their copyrights belong their original owners. These models may be used 
for educational and non-commercial purposes only and may not be re-distributed in any form.